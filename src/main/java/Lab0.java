

import javax.swing.*;
import observer.CountDao;
import observer.OvalFrame;
import observer.RectFrame;
import observer.TextFrame;
import subject.CounterSubject;

/**
 * A basic JFC 1.1 based application.
 */

public class Lab0 extends javax.swing.JFrame {
    private CounterSubject counterSubjectSubject;
    
	public Lab0() {
		setTitle("Count Application. (v2)");
		setDefaultCloseOperation(javax.swing.JFrame.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		setSize(255,143);
		setVisible(false);
		
		JButtonIncrease.setText("+");
		getContentPane().add(JButtonIncrease);
		JButtonIncrease.setBounds(24,60,87,30);
		
		JButtonDecrease.setText("-");
		getContentPane().add(JButtonDecrease);
		JButtonDecrease.setBounds(144,60,87,30);
		
		// Create counterSubjectSubject; and various view-frames, each linked to it.
		counterSubjectSubject = new CounterSubject();

    observers();

		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
		SymAction lSymAction = new SymAction();
		JButtonIncrease.addActionListener(lSymAction);
		JButtonDecrease.addActionListener(lSymAction);
	}

  private void observers() {
    TextFrame textf = new TextFrame(counterSubjectSubject);
    textf.setVisible(true);
    textf.setBounds(255, 0, 300, 200);

    RectFrame rectf = new RectFrame(counterSubjectSubject);
    rectf.setVisible(true);
    rectf.setBounds(255, 200, 300, 200);

    OvalFrame ovalf = new OvalFrame(counterSubjectSubject);
    ovalf.setVisible(true);
    ovalf.setBounds(255, 400, 300, 200);

    counterSubjectSubject.addObserver(new CountDao());
  }

  static public void main(String args[]) {
		try {
		    try {
		        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		    } catch (Exception e) {
		    }

			//Create a new instance of our application's frame, and make it visible.
			(new Lab0()).setVisible(true);
			//(new JFrameCounter("Count Application")).setVisible(true);
		} 
		catch (Throwable t) {
			t.printStackTrace();
			//Ensure the application exits with an error condition.
			System.exit(1);
		}
	}


	//{{
	javax.swing.JButton JButtonIncrease = new javax.swing.JButton();
	javax.swing.JButton JButtonDecrease = new javax.swing.JButton();
	//}}

	void exitApplication()
	{
		try {
		    	this.setVisible(false);    // hide the Frame
		    	this.dispose();            // free the system resources
		    	System.exit(0);            // close the application
		} catch (Exception e) {
		}
	}

	class SymWindow extends java.awt.event.WindowAdapter
	{
		public void windowClosing(java.awt.event.WindowEvent event)
		{
			Object object = event.getSource();
			if (object == Lab0.this)
				JFrameCounter_windowClosing(event);
		}
	}

	void JFrameCounter_windowClosing(java.awt.event.WindowEvent event)
	{
			 
		JFrameCounter_windowClosing_Interaction1(event);
	}

	void JFrameCounter_windowClosing_Interaction1(java.awt.event.WindowEvent event) {
		try {
			this.exitApplication();
		} catch (Exception e) {
		}
	}

	class SymAction implements java.awt.event.ActionListener
	{
		public void actionPerformed(java.awt.event.ActionEvent event)
		{
			Object object = event.getSource();
			if (object == JButtonIncrease)
				JButtonIncrease_actionPerformed(event);
			else if (object == JButtonDecrease)
				JButtonDecrease_actionPerformed(event);
		}
	}

	void JButtonIncrease_actionPerformed(java.awt.event.ActionEvent event) {
		counterSubjectSubject.increment();
	}

	void JButtonDecrease_actionPerformed(java.awt.event.ActionEvent event) {
		counterSubjectSubject.decrement();
	}
}
