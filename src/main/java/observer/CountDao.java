package observer;

import subject.CounterSubject;
import subject.Subject;

/**
 * Created by prayagupd
 * on 4/12/15.
 */

public class CountDao extends AbstractObserver {

  @Override
  public void update(Subject subj) {
    setCount(((CounterSubject) subj).getCountVal());
    insert();
  }

  private void insert() {
    System.out.println("inserting count "+ getCount() +" into database");
    //insert into database
  }
}
