package observer;

import java.awt.*;
import subject.CounterSubject;
import subject.Subject;

public class TextFrame extends javax.swing.JFrame implements Observer {
	public TextFrame(CounterSubject counterSubject)
	{
		//{{
		getContentPane().setLayout(null);
		setSize(227,196);
		setVisible(false);
		
		getContentPane().add(JLabelCount);
		JLabelCount.setFont(new Font("Dialog", Font.BOLD, 36));
		JLabelCount.setBounds(48,48,170,86);
		
		setTitle("Text Frame");
		
		// Initialize count display
		setCount(counterSubject);
		
		// Connect to counterSubject as an observer...
		counterSubject.addObserver(this);
		//}}

		//{{
		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
		//}}
	}


	class SymWindow extends java.awt.event.WindowAdapter
	{
		public void windowClosing(java.awt.event.WindowEvent event)
		{
		Object object = event.getSource();
		if (object == TextFrame.this)
			TextFrame_WindowClosing(event);
		}
	}

	void TextFrame_WindowClosing(java.awt.event.WindowEvent event)
	{
		dispose();		 // dispose of the Frame.
	}
	//{{
	javax.swing.JLabel JLabelCount = new javax.swing.JLabel();
	//}}

    public void setCount (Subject counterSubj){
        System.out.println(((CounterSubject)counterSubj).getCountVal());
        JLabelCount.setText(String.valueOf(((CounterSubject)counterSubj).getCountVal()));
        //repaint();
    }     
    
	// Process update event
	public void update( Subject cntr) {
		setCount( (CounterSubject)cntr );
	}
	
	// Update local view of counter, and refresh view
    public void setCount (CounterSubject counterSubjectSubj){
    	int count= counterSubjectSubj.getCountVal();
    	
    	//repaint();
    	JLabelCount.setText(String.valueOf( count ));
    }
   
}