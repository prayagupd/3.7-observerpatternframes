package observer;

import java.awt.*;
import subject.CounterSubject;
import subject.Subject;

public class RectFrame extends javax.swing.JFrame implements Observer {
  private int count;
    
	public RectFrame(CounterSubject counterSubject) {
		getContentPane().setLayout(null);
		setSize(300,200);
		setTitle("Rectangle Frame");
		
		// Initialize count display
		setCount(counterSubject);
		
		// Connect to counterSubject as an observer...
		counterSubject.addObserver(this);

		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);

	}

	class SymWindow extends java.awt.event.WindowAdapter
	{
		public void windowClosing(java.awt.event.WindowEvent event)
		{
		Object object = event.getSource();
		if (object == RectFrame.this)
			RectFrame_WindowClosing(event);
		}
	}

	void RectFrame_WindowClosing(java.awt.event.WindowEvent event) 	{
		dispose();		 // dispose of the Frame.
	}
	
	// Process update event
	public void update( Subject cntr) {
		setCount( (CounterSubject)cntr );
	}
	
	// Update local view of counter, and refresh view
    public void setCount (CounterSubject counterSubjectSubj){
    	count= counterSubjectSubj.getCountVal();
    	repaint();
    }
   

  public  void  paint(  Graphics  display  ) {
      // clear display first
      Dimension sizes = getSize();
      display.clearRect(0,0,sizes.width,sizes.height);

      int x = 50;
      int y = 30;

      // Magnify value by 5 to get a bigger visual effect
      int height =  count*9;
      int width = count*9;

      display.setColor( Color.red );

      display.fillRect(x, y, Math.abs(width), Math.abs( height ) );
  }
}