package observer;

import subject.Subject;

/**
 * Created by prayagupd
 * on 4/12/15.
 */

public abstract class AbstractObserver implements Observer {
  private int count;

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }
}
