package subject;

import java.util.ArrayList;
import java.util.List;
import observer.*;

public abstract class AbstractSubject implements Subject {
    private List<Observer> observerList=new ArrayList<Observer>();
    
    public void addObserver(observer.Observer observer) {
    	observerList.add(observer);
    }
    
    public void removeObserver(observer.Observer observer)  {
    	observerList.remove(observer);
    }
    
    public void notifyObservers() {
        for ( Observer o : observerList )
            o.update(this);
    }
}