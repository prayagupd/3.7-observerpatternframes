package subject;

public class CounterSubject extends AbstractSubject {
    private int count;

    public CounterSubject() {
        count = 0;
    }

    //IncrementCommand
    public void increment() {
        count++;
        notifyObservers();
    }

    //DecrementCommand
    public void decrement() {
        if ( count >0 ) {
            count--;
            notifyObservers();
        }
    }
    
    public int getCountVal() {
        return count;
    }
}